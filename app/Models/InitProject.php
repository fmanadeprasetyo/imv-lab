<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InitProject extends Model
{
    protected $fillable = [
        'project_name'
    ];

    public function member(){
        return $this->hasMany(\App\Models\Member::class, 'id_project');
    }
}
