<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proffesor extends Model
{
    protected $casts = [
        'id_project' => 'int',
        'gender' => 'int'
    ];

    protected $fillable = [
        'name',
        'id_project',
        'email',
        'instagram',
        'gender',
        'url_photo',
    ];

    public function init_project(){
        return $this->belongsTo(\App\Models\InitProject::class, 'id_project');
    }
    
    public function init_gender(){
        return $this->belongsTo(\App\Models\InitGender::class, 'gender');
    }
}
