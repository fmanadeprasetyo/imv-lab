<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InitDivition extends Model
{
    protected $fillable = [
        'divition_name'
    ];

    public function divition(){
        return $this->hasMany(\App\Models\Divition::class, 'id_divition');
    }
}
