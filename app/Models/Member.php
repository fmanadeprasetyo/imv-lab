<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $casts = [
        'gender' => 'int',
        'id_divition' => 'int', 
        'id_project' => 'int'
    ];

    protected $fillable = [
        'name',
        'id_divition',
        'id_project',
        'email',
        'instagram',
        'gender',
        'url_photo'
    ];

    public function init_divition(){
        return $this->belongsTo(\App\Models\InitDivition::class, 'id_divition');
    }

    public function init_project(){
        return $this->belongsTo(\App\Models\InitProject::class, 'id_project');
    }

    public function init_gender(){
        return $this->belongsTo(\App\Models\InitGender::class, 'gender');
    }
}
