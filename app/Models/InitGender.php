<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InitGender extends Model
{
    protected $fillable = [
        'gender_name'
    ];

    public function member(){
        return $this->hasMany(\App\Models\Member::class, 'gender');
    }
    
    public function proffesor(){
        return $this->hasMany(\App\Models\Proffesor::class, 'gender');
    }
}
